package com.asok.dev.minesweeper.Db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

public class SqlDB extends SQLiteOpenHelper {

    //Initialize the local DB
    public SqlDB(Context context) {
        super(context, "minesweeper.db", null, 1);
    }

    //Fires on first create in the context
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table playerInfo " +
                        "(player_id integer primary key autoincrement, player_name text, player_score integer)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS playerInfo");
        onCreate(db);
    }

    //Insert new score if its not available in the DB
    public void insertScore(String playerName, String playerScore) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from playerInfo where player_name =" + "'" + playerName + "'", null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            int score = Integer.parseInt(cursor.getString(cursor.getColumnIndex("player_score"))) + 10;
            updateScore(playerName, Integer.toString(score));
        } else {
            SQLiteDatabase dbWritable = this.getReadableDatabase();
            ContentValues parameters = new ContentValues();
            parameters.put("player_name", playerName);
            parameters.put("player_score", playerScore);
            dbWritable.insert("playerInfo", null, parameters);
        }
    }

    //Update the score
    public void updateScore(String playerName, String playerScore) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues parameters = new ContentValues();
        parameters.put("player_score", playerScore);
        db.update("playerInfo", parameters, "player_name = ? ", new String[]{playerName});

    }

    //Get all the scores from the DB
    public HashMap<String, String> getAllCotacts() {
        HashMap<String, String> dictionary = new HashMap<String, String>();

        SQLiteDatabase sql = this.getReadableDatabase();
        Cursor cursor = sql.rawQuery("select * from playerInfo", null);
        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {
            dictionary.put(cursor.getString(cursor.getColumnIndex("player_name")), cursor.getString(cursor.getColumnIndex("player_score")));
            cursor.moveToNext();
        }
        return dictionary;
    }
}
