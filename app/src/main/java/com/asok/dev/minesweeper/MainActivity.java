package com.asok.dev.minesweeper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asok.dev.minesweeper.core.GameEngine;

import java.util.Map;

//This class is responsible for rendering UI
public class MainActivity extends Activity {

    //declarations
    public TableLayout tblMineMapArea;
    //public TextView txtMineCounter;
    public TextView txtTimer;
    //timer
    int counter = 0;
    private CountDownTimer timer;
    private boolean isStarted;
    private int difficultyLevels = 9;
    private String userName;

    //On application load -Logic
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView txtMineCounter = (TextView) findViewById(R.id.MineCounter);
        txtMineCounter.setText("0" + Integer.toString(difficultyLevels));

        //Timer event to handle the start and stop.
        timer = new CountDownTimer(9999000, 1000) {
            //timer ON
            @Override
            public void onTick(long millisUntilFinished) {
                counter++;
                txtTimer.setText(Integer.toString(counter));
            }

            //timer OFF
            //Notification to the user when the timer stops
            @Override
            public void onFinish() {
                ToastMessage("Time is Over. Click start for a new Game");
            }
        };
        GetUserName();
    }

    //Takes care of setting difficulty levels
    public void OnSettingsButtonClick(View view) {
        Button btnSettings = (Button) findViewById(R.id.btnSettings);
        PopupMenu difficultyLevels = new PopupMenu(MainActivity.this, btnSettings);

        difficultyLevels.getMenuInflater().inflate(R.menu.diffuculty_level_menu, difficultyLevels.getMenu());

        difficultyLevels.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            public boolean onMenuItemClick(MenuItem item) {
                ToastMessage("You Selected : " + item.getTitle());
                SetLevel(item.getTitle().toString());
                return true;
            }
        });
        difficultyLevels.show();
    }

    //Get the user Name of player
    private void GetUserName() {
        AlertDialog.Builder alertBox = new AlertDialog.Builder(MainActivity.this);
        alertBox.setTitle("Welcome to Minesweeper Game");

        alertBox.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int id) {
                Dialog ok = (Dialog) dialog;
                EditText loginName = ok.findViewById(R.id.dialogUserName);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(loginName, InputMethodManager.SHOW_IMPLICIT);
                userName = loginName.getText().toString().trim();
                dialog.dismiss();
                ToastMessage("Choose the difficulty level by clicking on Settings. By default difficulty level set to 'Beginner'");
                ToastMessage("Click on Start for a new game");
            }
        });
        LayoutInflater inflater = getLayoutInflater();
        View inflatedDialog = inflater.inflate(R.layout.custom_alert_box, null);
        alertBox.setView(inflatedDialog);
        alertBox.show();
    }

    //Start a new game on select of start button click
    public void OnStartResetButtonClick(View view) {
        isStarted = true;
        timer.cancel();
        counter = 0;
        setContentView(R.layout.activity_main);
        GameEngine.Instance(this).GenerateMineMapGrid(this, timer, isStarted, difficultyLevels, userName);
        TextView txtMineCounter = (TextView) findViewById(R.id.MineCounter);
        txtMineCounter.setText("0" + Integer.toString(difficultyLevels));
        txtTimer = (TextView) findViewById(R.id.Timer);
        timer.start();
    }

    //Click event listener for score display
    public void OnScoreButtonClick(View view) {
        Button btnScore = (Button) findViewById(R.id.btnScore);
        PopupMenu difficultyLevels = new PopupMenu(MainActivity.this, btnScore);
        Map<String, String> scores = GameEngine.Instance(this).GetScores();
        if (scores.isEmpty()) {
            difficultyLevels.getMenu().add(1, R.id.Beginner, 1, "Win Atleast one Game!");
        }
        for (Map.Entry<String, String> item : scores.entrySet()) {
            difficultyLevels.getMenu().add(1, R.id.Beginner, 1, item.getKey() + "  -  " + item.getValue() + " points");
        }
        difficultyLevels.show();
    }

    //Displays Notifications to the user
    private void ToastMessage(String message) {
        Context context = getApplicationContext();
        CharSequence msg = message;
        int duration = Toast.LENGTH_LONG;
        //display message to the user upon timer STOP
        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }

    //set the difficulty level
    private void SetLevel(String levels) {
        switch (levels) {
            case "Beginner":
                difficultyLevels = 9;
                break;
            case "Intermediate":
                difficultyLevels = 25;
                break;
            case "Advanced":
                difficultyLevels = 41;
                break;
        }
    }
}
