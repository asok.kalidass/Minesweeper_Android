package com.asok.dev.minesweeper.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.widget.Toast;

import com.asok.dev.minesweeper.Db.SqlDB;
import com.asok.dev.minesweeper.view.MineMapCell;

import java.util.HashMap;

//This class is responsible for handling minesweeper logic
public class GameEngine {
    private static GameEngine instance;
    private static SqlDB playerInfo;
    private CountDownTimer timer;
    public int bombCount;
    public static final int ROW_COUNT = 9;
    public static final int COLUMN_COUNT = 9;
    private MineMapCell[][] mineMapCells;
    public boolean isStarted;
    private boolean isGameWon;
    private String userName;
    private Context context;

    private GameEngine() {
        //Do Nothing
    }

    //Initialte the application on click of start button
    public static GameEngine Instance(Context context) {
        if (instance == null) {
            instance = new GameEngine();
        }
        return instance;
    }

    //Initialte the application on click of start button
    public static GameEngine Instance() {
        if (instance == null) {
            instance = new GameEngine();
        }
        return instance;
    }

    //Constructor
    public void GenerateMineMapGrid(Context context, CountDownTimer timer, boolean isStarted, int bombCount, String userName) {
        this.userName = userName;
        this.context = context;
        this.bombCount = bombCount;
        this.timer = timer;
        this.isStarted = isStarted;
        mineMapCells = new MineMapCell[ROW_COUNT][COLUMN_COUNT];
        MineMapGenerator mineMapGenerator = new MineMapGenerator(ROW_COUNT, COLUMN_COUNT, bombCount);
        int[][] generatedMineMap = mineMapGenerator.GenerateMineMap();
        setCellValue(context, generatedMineMap);
    }

    //Constructor
    public void GenerateMineMapGrid(Context context) {
        this.context = context;
        mineMapCells = new MineMapCell[ROW_COUNT][COLUMN_COUNT];
        MineMapGenerator mineMapGenerator = new MineMapGenerator(ROW_COUNT, COLUMN_COUNT, bombCount);
        int[][] generatedMineMap = mineMapGenerator.GenerateMineMap();

        setCellValue(context, generatedMineMap);
    }

    //Populate the grid cell value
    private void setCellValue(final Context context, final int[][] grid) {
        for (int i = 0; i < ROW_COUNT; i++) {
            for (int j = 0; j < COLUMN_COUNT; j++) {
                if (mineMapCells[i][j] == null) {
                    mineMapCells[i][j] = new MineMapCell(context, i, j);
                }
                mineMapCells[i][j].setValue(grid[i][j]);
                mineMapCells[i][j].invalidate();
            }
        }
    }

    //Get the cell value based on the position
    public MineMapCell getCellValue(int position) {
        int x = position % ROW_COUNT;
        int y = position / COLUMN_COUNT;

        return mineMapCells[x][y];
    }

    //Get the individual cell value
    public MineMapCell getCellValue(int x, int y) {
        return mineMapCells[x][y];
    }

    //Handle the click logic
    boolean isRecursive = true;
    int count = 0;

    public boolean click(int x, int y) {
        isGameWon = false;
        if (x >= 0 && y >= 0 && x < ROW_COUNT && y < COLUMN_COUNT && !getCellValue(x, y).isClicked()) {
            getCellValue(x, y).setClicked();
            getCellValue(x, y).setCellRevealed();
            if (getCellValue(x, y).getValue() == 0) {
                for (int xt = -1; xt <= 1; xt++) {
                    for (int yt = -1; yt <= 1; yt++) {
                        if (xt != yt) {
                            click(x + xt, y + yt);
                        } else {
                            isRecursive = false;
                        }
                    }
                }
            }

            if (getCellValue(x, y).isBomb()) {
                onGameLost(x, y);
            }
        }
        checkEnd();
        return isGameWon;
    }

    //check for cells that are revealed and not revealed
    private boolean checkEnd() {
        int bombNotFound = bombCount;
        int notRevealed = (ROW_COUNT * COLUMN_COUNT);
        for (int x = 0; x < ROW_COUNT; x++) {
            for (int y = 0; y < COLUMN_COUNT; y++) {
                if (getCellValue(x, y).isCellRevealed() || getCellValue(x, y).isFlagged()) {
                    notRevealed--;
                }

                if (getCellValue(x, y).isFlagged() && getCellValue(x, y).isBomb()) {
                    bombNotFound--;
                }
            }
        }

        if (notRevealed == bombCount) {
            isGameWon = true;
        }
        return false;
    }

    //log the score into DB
    public void logintoDatabase() {
        playerInfo = new SqlDB(context);
        playerInfo.insertScore(userName, "10");
        Toast.makeText(context, "Game won", Toast.LENGTH_LONG).show();
        timer.cancel();
    }

    public HashMap<String, String> GetScores() {
        playerInfo = new SqlDB(context);
        return playerInfo.getAllCotacts();
    }

    //Onlog click logic
    public void flag(int x, int y) {
        boolean isFlagged = getCellValue(x, y).isFlagged();
        getCellValue(x, y).setFlagged(!isFlagged);
        getCellValue(x, y).invalidate();
    }

    //Logic to perform on Game Lost scenario
    private void onGameLost(int x, int y) {
        timer.cancel();
        // handle lost game
        Toast.makeText(context, "Game lost", Toast.LENGTH_LONG).show();
        for (int row = 0; row < ROW_COUNT; row++) {
            for (int col = 0; col < COLUMN_COUNT; col++) {
                getCellValue(row, col).setRevealedOnGameLost();
                getCellValue(row, col).setOnGameLost();
            }
        }
    }
}
