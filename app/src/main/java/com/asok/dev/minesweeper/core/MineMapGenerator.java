package com.asok.dev.minesweeper.core;

import java.util.Random;

//This class is responsible for mine settings anf generating array of neighbours
public class MineMapGenerator {

    private int numRows, numColumns, numBombs;
    //Initialize
    private int[][] MineMapGrid;

    public MineMapGenerator(final int nrows, final int ncolumns, final int nbombs) {
        numRows = nrows;
        numColumns = ncolumns;
        numBombs = nbombs;
    }

    //set bomb and neighbours count
    public int[][] GenerateMineMap() {
        MineMapGrid = InitializeMineMap();
        SetBombsInMineMap();
        SetNeighbourBombsCount();
        return MineMapGrid;
    }

    //Assign the neighbouring bomb count to the cell
    private void SetNeighbourBombsCount() {
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numColumns; j++) {
                MineMapGrid[i][j] = GetBombsCount(i, j);
            }
        }
    }

    //Gets the bomb count
    private int GetBombsCount(int row, int col) {
        if (HasBomb(row, col)) return -1;
        int bombCount = 0;
        //Check for bomb in top left
        if (HasBomb(row - 1, col - 1)) {
            bombCount++;
        }
        //Check for bomb in top middle
        if (HasBomb(row - 1, col)) {
            bombCount++;
        }
        //Check for bomb in top right
        if (HasBomb(row - 1, col + 1)) {
            bombCount++;
        }
        //Check for bomb in Middle left
        if (HasBomb(row, col - 1)) {
            bombCount++;
        }
        //Check for bomb in middle right
        if (HasBomb(row, col + 1)) {
            bombCount++;
        }
        //check for bomb in top left
        if (HasBomb(row + 1, col - 1)) {
            bombCount++;
        }
        //Check for bomb in top middle
        if (HasBomb(row + 1, col)) {
            bombCount++;
        }
        ////Check for bomb in top right
        if (HasBomb(row + 1, col + 1)) {
            bombCount++;
        }

        return bombCount;
    }

    //Randomly generate the bomb and set it in the grid cell randomly
    private void SetBombsInMineMap() {
        Random rand;
        rand = new Random();
        int randRow, randCol, nBombs = numBombs;

        while (nBombs > 0) {
            randRow = rand.nextInt(numRows);
            randCol = rand.nextInt(numColumns);

            if (MineMapGrid[randRow][randCol] != -1) {
                MineMapGrid[randRow][randCol] = -1;
                nBombs--;
            }
        }
    }

    //Set the bool variable if the cell has bomb ( bomb is denoted by -1)
    public boolean HasBomb(int row, int col) {
        if (row >= 0 && col >= 0 && row < numRows && col < numColumns) {
            if (MineMapGrid[row][col] == -1) return true;
        }
        return false;
    }

    //Initialize the mine map
    private int[][] InitializeMineMap() {
        int[][] mineMapGrid = new int[numRows][numColumns];

        for (int i = 0; i < numRows; i++)
            mineMapGrid[i] = new int[numColumns];

        return mineMapGrid;
    }
}
