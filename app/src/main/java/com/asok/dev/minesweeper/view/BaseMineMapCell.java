package com.asok.dev.minesweeper.view;

import android.content.Context;
import android.view.View;

import com.asok.dev.minesweeper.core.GameEngine;

public abstract class BaseMineMapCell extends View {

    private int value;
    private boolean isBomb;
    private boolean isRevealed;
    private boolean isClicked;
    private boolean isFlagged;
    private boolean isGameOver;
    private boolean isCellRevealed;

    private int x, y;
    private int position;

    public BaseMineMapCell(Context context) {
        super(context);
    }

    public int getValue() {
        return value;
    }

    //Initiate the grid cell properties
    public void setValue(int value) {
        isBomb = false;
        isRevealed = false;
        isClicked = false;
        isFlagged = false;
        isCellRevealed = false;
        if (value == -1) {
            isBomb = true;
        }

        this.value = value;
    }

    //Getters and setters

    public boolean isBomb() {
        return isBomb;
    }

    public boolean isRevealed() {
        return isRevealed;
    }

    public void setRevealedOnGameLost() {
        isRevealed = true;
        invalidate();
    }
    //Call this setter whenever the cell is revealed
    public void setCellRevealed() {
        isCellRevealed = true;
        invalidate();
    }

    public boolean isCellRevealed() {
        return isCellRevealed;
    }

    public boolean isGameLost() {
        return isGameOver;
    }

    public void setOnGameLost() {
        isGameOver = true;
        invalidate();
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked() {
        this.isClicked = true;
        invalidate();
    }

    public boolean isFlagged() {
        return isFlagged;
    }

    public void setFlagged(boolean flagged) {
        isFlagged = flagged;
    }

    public int getXPos() {
        return x;
    }

    public int getYPos() {
        return y;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;

        this.position = y * GameEngine.ROW_COUNT + x;

        invalidate();
    }
}
