package com.asok.dev.minesweeper.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.asok.dev.minesweeper.core.GameEngine;

//The minemap class is responsible for binding content to gridview
public class MineMap extends GridView {
    //Constructor
    public MineMap(Context context, AttributeSet attrs) {
        //initiate base class
        super(context, attrs);

        GameEngine.Instance().GenerateMineMapGrid(context);

        setNumColumns(GameEngine.ROW_COUNT);
        //bond the data to the grid
        setAdapter(new GridAdapter());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    //This adapter class is responsible for List view implementation in grid
    private class GridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return GameEngine.ROW_COUNT * GameEngine.COLUMN_COUNT;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return GameEngine.Instance().getCellValue(position);
        }
    }
}
