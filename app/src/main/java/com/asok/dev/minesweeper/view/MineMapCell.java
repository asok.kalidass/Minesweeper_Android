package com.asok.dev.minesweeper.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.asok.dev.minesweeper.R;
import com.asok.dev.minesweeper.core.GameEngine;

//This class is responsible for handling grid events.
public class MineMapCell extends BaseMineMapCell implements View.OnClickListener, View.OnLongClickListener {
    private boolean isFlagged;
    private int row, col;
    private int position;
    public boolean isClicked;
    private Context context;
    private boolean isGameWon;

    //Constructor
    public MineMapCell(Context context, int x, int y) {
        super(context);
        this.context = context;

        setPosition(x, y);

        setOnClickListener(this);
        setOnLongClickListener(this);
    }

    //Getter and Setter properties
    public boolean isFlagged() {
        return isFlagged;
    }

    public void setFlagged(boolean flagged) {
        isFlagged = flagged;
    }

    public int getXPos() {
        return row;
    }

    public int getYPos() {
        return col;
    }

    public void setPosition(int x, int y) {
        this.row = x;
        this.col = y;

        this.position = y * GameEngine.ROW_COUNT + x;

        invalidate();
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
    //On click of grid cell
    @Override
    public void onClick(View v) {
        if (!isGameLost() && GameEngine.Instance().isStarted)
            isGameWon = GameEngine.Instance().click(getXPos(), getYPos());
        isClicked = true;
        if (isGameWon)
            GameEngine.Instance().logintoDatabase();
    }

    //On long click
    @Override
    public boolean onLongClick(View v) {
        if (GameEngine.Instance(context).isStarted) {
            GameEngine.Instance(context).flag(getXPos(), getYPos());
            isClicked = true;
            return true;
        }
        return false;
    }

    //On formation of grid
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawButton(canvas);

        if (isFlagged()) {
            drawFlag(canvas);
        } else if (isRevealed() && isBomb() && !isClicked()) {
            drawNormalBomb(canvas);
        } else {
            if (isClicked()) {
                if (getValue() == -1) {
                    drawBombExploded(canvas);

                } else {
                    drawNumber(canvas);
                }
            } else {
                drawButton(canvas);
            }
        }
    }

    //set the explored bomb image
    private void drawBombExploded(Canvas canvas) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.bomb_exploded);
        drawable.setBounds(0, 0, getWidth(), getHeight());
        drawable.draw(canvas);
    }

    //set the flag image
    private void drawFlag(Canvas canvas) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.flag);
        drawable.setBounds(0, 0, getWidth(), getHeight());
        drawable.draw(canvas);
    }

    //set the button on page load
    private void drawButton(Canvas canvas) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.button);
        drawable.setBounds(0, 0, getWidth(), getHeight());
        drawable.draw(canvas);
    }

    //set the unexplored bomb image
    private void drawNormalBomb(Canvas canvas) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.bomb_normal);
        drawable.setBounds(0, 0, getWidth(), getHeight());
        drawable.draw(canvas);
    }

    //Draw the grid layout bsed on the condition
    private void drawNumber(Canvas canvas) {
        Drawable drawable = null;

        switch (getValue()) {
            case 0:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_0);
                break;
            case 1:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_1);
                break;
            case 2:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_2);
                break;
            case 3:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_3);
                break;
            case 4:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_4);
                break;
            case 5:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_5);
                break;
            case 6:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_6);
                break;
            case 7:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_7);
                break;
            case 8:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_8);
                break;
        }
        drawable.setBounds(0, 0, getWidth(), getHeight());
        drawable.draw(canvas);
    }
}
